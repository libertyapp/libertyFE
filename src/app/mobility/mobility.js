angular.module('dashboard').controller('node1',function($scope, $filter, ngTableParams, $http, $stateParams,$rootScope) {
           $scope.options = {
            chart: {
                type: 'lineWithFocusChart',
                height: 450,
                //x: function(d) {return d3.time.format("%Y/%W").parse(d['x']);}, //(NOT WORKING - try for Y/W format)
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 70
                },
                duration: 500,
                useInteractiveGuideline: true,

                xAxis: {
                    axisLabel: 'X Axis',
                    tickFormat: function(d){
                        //return d3.time.format('%Y/%W')(new Date(d)) //(NOT WORKING - try for Y/W format)
                        return d3.format('d')(d);
                    }
                },
                xAxis: {
                        rotateLabels: -45,
                        tickFormat: function(d){
                            return $scope.ccLabels[d];
                        }
                    },
                x2Axis: {
                        axisLabel: 'Distance',
                        tickFormat: function(d){
                            return d3.format(',.2f')($scope.ccDistances[d]);
                        }
                    },

                yAxis: {
                    axisLabel: 'Y Axis',
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    },
                    rotateYLabel: false
                },
                y2Axis: {
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    }
                }

            }
        };

});
angular.module('dashboard').controller('node2',function($scope, $filter, ngTableParams, $http, $stateParams,$rootScope) {
           $scope.options = {
            chart: {
                type: 'lineWithFocusChart',
                height: 450,
                //x: function(d) {return d3.time.format("%Y/%W").parse(d['x']);}, //(NOT WORKING - try for Y/W format)
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 70
                },
                duration: 500,
                useInteractiveGuideline: true,

                xAxis: {
                    axisLabel: 'X Axis',
                    tickFormat: function(d){
                        //return d3.time.format('%Y/%W')(new Date(d)) //(NOT WORKING - try for Y/W format)
                        return d3.format('d')(d);
                    }
                },
                xAxis: {
                        rotateLabels: -45,
                        tickFormat: function(d){
                            return $scope.rcLabels[d];
                        }
                    },
                x2Axis: {
                        axisLabel: 'Distance',
                        tickFormat: function(d){
                            return d3.format(',.2f')($scope.rcDistances[d]);
                        }
                    },

                yAxis: {
                    axisLabel: 'Y Axis',
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    },
                    rotateYLabel: false
                },
                y2Axis: {
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    }
                }

            }
        };

});
angular.module('dashboard').controller('node3',function($scope, $filter, ngTableParams, $http, $stateParams,$rootScope) {
    $scope.ccscope = 5;
});

angular.module('dashboard').controller(
		'mobility',
		function($scope, $filter, ngTableParams, $http, $stateParams,
				$rootScope) {
        var secs = {"3d":259200,"1w":604800,"2w":1209600,"3w":1814400,"1m":2629743,"2m":5259487,"3m":7889231,"6m":15778463,"9m":23667694,"1y":31556926};

        $scope.percentview = false;
        $scope.range = '1y';
        $scope.approvedRange = '1y';
        $scope.dataRange = '1y';
        $scope.fulldata = [];
        $scope.changeView = function(percentview) {
            $scope.percentview = percentview;
            $scope.changeRange($scope.range);
        }

			
			$http.get("/api/get_regions").then(
					function(response) {
						$scope.adata = response.data.regions;
					
                        $scope.tableParams.reload();
					});

            $scope.automaton = "";
            $scope.getClients = function(automaton_id, name) {
                $scope.automaton = name;
                $scope.clients = [];
			    $http.get("/api/get_clients_by_aid/" + automaton_id).then(function(response) {
                    $scope.clients = response.data.output; 
                    $scope.clients.forEach(function(e) {
                        e.reviewed = new Date(e.reviewed*1000);
                    });
                });
                
            };
            $scope.data = [];
        
           $scope.options = {
            chart: {
                type: 'lineWithFocusChart',
                height: 450,
                //x: function(d) {return d3.time.format("%Y/%W").parse(d['x']);}, //(NOT WORKING - try for Y/W format)
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 70
                },
                duration: 500,
                useInteractiveGuideline: true,

                xAxis: {
                    axisLabel: 'X Axis',
                    tickFormat: function(d){
                        //return d3.time.format('%Y/%W')(new Date(d)) //(NOT WORKING - try for Y/W format)
                        return d3.format('d')(d);
                    }
                },
                xAxis: {
                        rotateLabels: -45,
                        tickFormat: function(d){
                            return $scope.labels[d];
                        }
                    },
                x2Axis: {
                        axisLabel: 'Distance',
                        tickFormat: function(d){
                            return d3.format(',.2f')($scope.distances[d]);
                        }
                    },

                yAxis: {
                    axisLabel: 'Y Axis',
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    },
                    rotateYLabel: false
                },
                y2Axis: {
                    tickFormat: function(d){
                        return d3.format(',.2f')(d);
                    }
                }

            }
        };
    
            
        $scope.name = "";
        $scope.type = "distance";
        $scope.load_data = function(type) {
            $scope.type = type;
            $http.get("/api/get_mobility/10022/" + $scope.type).then(function(response) {
                $scope.data = response.data.data;
                $scope.labels = response.data.labels;
                $scope.distances = response.data.distances;
            });
            $http.get("/api/get_mobility/11525/" + $scope.type).then(function(response) {
                $scope.ccData = response.data.data;     
                $scope.ccLabels = response.data.labels;     
                $scope.ccDistances = response.data.distances;     
            });
            $http.get("/api/get_mobility/11177/" + $scope.type).then(function(response) {
                $scope.rcData = response.data.data;     
                $scope.rcLabels = response.data.labels;     
                $scope.rcDistances = response.data.distances;     
            });
        };
        $scope.load_data($scope.type);
        console.log($stateParams.automaton_id);
        if (!isNaN($stateParams.automaton_id)) {
            
            $scope.load_data($stateParams.automaton_id, $stateParams.name);
        }
		})
