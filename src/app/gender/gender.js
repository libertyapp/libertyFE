angular.module('dashboard').controller('gender',
		function($scope, $filter, ngTableParams, $http, $stateParams,
				$rootScope) {
        var secs = {"3d":259200,"1w":604800,"2w":1209600,"3w":1814400,"1m":2629743,"2m":5259487,"3m":7889231,"6m":15778463,"9m":23667694,"1y":31556926};

        $scope.percentview = false;
        $scope.range = '1y';
        $scope.approvedRange = '1y';
        $scope.dataRange = '1y';
        $scope.fulldata = [];


			$scope.statuses = [];
			$scope.statuses.push({
				id : "",
				title : "all"
			});
			$scope.automaton_purpose = [];
			$scope.automaton_purpose.push({
				id : "",
				title : "all"
			});
			$scope.execution_purpose = [];
			$scope.execution_purpose.push({
				id : "",
				title : "all"
			});

			$scope.redeployable = [];
			$scope.redeployable.push({
				id : "",
				title : "all"
			});
            
            $scope.tableParams = new ngTableParams({
                page : 1, // show first page
                count : 10, // count per page
            }, {
                getData : function($defer, params) {
                    // use build-in angular filter
                    filteredData = params.filter() ? $filter(
                            'filter')($scope.adata, params.filter())
                            : tabledata;

                    var orderedData = params.sorting() ? $filter(
                            'orderBy')(filteredData,
                            params.orderBy()) : filteredData;
                    params.total(filteredData.length);
                    var page = orderedData.slice(
                            (params.page() - 1) * params.count(),
                            params.page() * params.count());
                    $scope.tabledata = page;
                    $defer.resolve(page);
                }
            });
			
			$http.get("/api/get_regions").then(
					function(response) {
						$scope.adata = response.data.regions;
					
                        $scope.tableParams.reload();
					});

            $scope.automaton = "";
            $scope.data = [];
        $scope.options = {  
            chart: {  
                type: 'multiBarHorizontalChart',  
                height: 650,  
                x: function (d) { return d.label; },  
                y: function (d) { return d.value; },  
                showControls: true,  
                showValues: false,  
                duration: 500,  
                xAxis: {  
                    axisLabel: 'Hour',  
                    showMaxMin: false  
                },  
                yAxis: {  
                    axisLabel: 'People Count',  
                    tickFormat: function (d) {  
                        return d3.format('.1')(d);  
                    }  
                }  
            }  
        };  
        $scope.name = "";
        $scope.load_data = function(region_id, name) {
            $scope.name = name;
			$http.get("/api/get_gender/" + region_id).then(function(response) {
                //$scope.tickValues = response.data.output.dates;
                //var lr = linearRegression(response.data.output.stats[4].values);
                //response.data.output.stats.push({"key":"lr","values":lr}); 
                //$scope.fulldatapc = response.data.output.pcstats;
                $scope.data = response.data.data;
                //$scope.history = response.data.output.history;
                //$scope.changeRange($scope.range);
                //$scope.api.refresh();
            });
        };
        console.log($stateParams.automaton_id);
        if (!isNaN($stateParams.automaton_id)) {
            
            $scope.load_data($stateParams.automaton_id, $stateParams.name);
        }
		})
