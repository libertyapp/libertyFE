angular.module('dashboard').controller('ccChart',function($scope, $filter, ngTableParams, $http, $stateParams,$rootScope) {
    $scope.options = {
       // .nvd3 .nv-group.nv-series-0 {
        //      fill: lightgray!important; /* color for area */
        //  }
        chart: {
            //color:  d3.scale.category20c(),
            type: 'stackedAreaChart',
            showControls: true,
            controlOptions: ["Stacked","Expanded"],
            height: 450,
            margin : {
                top: 20,
                right: 20,
                bottom: 30,
                left: 70
            },
            x: function(d){return d[0];},
            y: function(d){return d[1];},
            useVoronoi: false,
            clipEdge: true,
            duration: 500,
            noData: 'No data',
//            color: ["#FF0000", "#E6D72A", "#3F681C", "#6FB98F", "#FF9900", "#375E97", "#ffff66" , "#021C1E"],
            useInteractiveGuideline: true,
            xAxis: {
                showMaxMin: false,
                axisLabelUseCanvas: true,
                tickFormat: function(d){
                    return d3.format('.1')(d);
                }
            },
            yAxis: {
                axisLabelUseCanvas: true,
                tickFormat: function(d){
                    return d3.format(',.2f')(d);
                }
            },
//                zoom: {
//                    enabled: true,
//                    scaleExtent: [1, 10],
//                    useFixedDomain: false,
//                    useNiceScale: false,
//                    horizontalOff: false,
//                    verticalOff: true,
//                    unzoomEventType: 'dblclick.zoom'
//                }
        }
    };

});
angular.module('dashboard').controller('rcChart',function($scope, $filter, ngTableParams, $http, $stateParams,$rootScope) {
    $scope.options = {
       // .nvd3 .nv-group.nv-series-0 {
        //      fill: lightgray!important; /* color for area */
        //  }
        chart: {
            //color:  d3.scale.category20c(),
            type: 'stackedAreaChart',
            showControls: true,
            controlOptions: ["Stacked","Expanded"],
            height: 450,
            margin : {
                top: 20,
                right: 20,
                bottom: 30,
                left: 70
            },
            x: function(d){return d[0];},
            y: function(d){return d[1];},
            useVoronoi: false,
            clipEdge: true,
            duration: 500,
            noData: 'No data',
            useInteractiveGuideline: true,
            xAxis: {
                showMaxMin: false,
                axisLabelUseCanvas: true,
                tickFormat: function(d){
                    return d3.format('.1')(d);
                }
            },
            yAxis: {
                axisLabelUseCanvas: true,
                tickFormat: function(d){
                    return d3.format(',.2f')(d);
                }
            },
//                zoom: {
//                    enabled: true,
//                    scaleExtent: [1, 10],
//                    useFixedDomain: false,
//                    useNiceScale: false,
//                    horizontalOff: false,
//                    verticalOff: true,
//                    unzoomEventType: 'dblclick.zoom'
//                }
        }
    };

});
angular.module('dashboard').controller('exeChart',function($scope, $filter, ngTableParams, $http, $stateParams,$rootScope) {
    $scope.ccscope = 5;
});

angular.module('dashboard').controller(
		'qacomp',
		function($scope, $filter, ngTableParams, $http, $stateParams,
				$rootScope) {
        var secs = {"3d":259200,"1w":604800,"2w":1209600,"3w":1814400,"1m":2629743,"2m":5259487,"3m":7889231,"6m":15778463,"9m":23667694,"1y":31556926};

        $scope.percentview = false;
        $scope.range = '1y';
        $scope.approvedRange = '1y';
        $scope.dataRange = '1y';
        $scope.fulldata = [];
        $scope.changeView = function(percentview) {
            $scope.percentview = percentview;
            $scope.changeRange($scope.range);
        }

            
            $scope.tableParams = new ngTableParams({
                page : 1, // show first page
                count : 10, // count per page
            }, {
                getData : function($defer, params) {
                    // use build-in angular filter
                    filteredData = params.filter() ? $filter(
                            'filter')($scope.adata, params.filter())
                            : tabledata;

                    var orderedData = params.sorting() ? $filter(
                            'orderBy')(filteredData,
                            params.orderBy()) : filteredData;
                    params.total(filteredData.length);
                    var page = orderedData.slice(
                            (params.page() - 1) * params.count(),
                            params.page() * params.count());
                    $scope.tabledata = page;
                    $defer.resolve(page);
                }
            });
			
			$http.get("/api/get_regions").then(
					function(response) {
						$scope.adata = response.data.regions;
					
                        $scope.tableParams.reload();
					});

            $scope.data = [];
                $scope.options = {
                chart: {
                    type: 'lineWithFocusChart',
                    padData: true,
                    height: 450,
                    //x: function(d) {return d3.time.format("%Y/%W").parse(d['x']);}, //(NOT WORKING - try for Y/W format)
                    margin : {
                        top: 20,
                        right: 20,
                        bottom: 80,
                        left: 70
                    },
                    duration: 500,
                    useInteractiveGuideline: true,
                    noData: 'No data',
                    color: ["#F70000","#73B02E", "#FFA500", "#1E90FF", "#8B4513", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000"],

                    xAxis: {
                        axisLabel: 'X Axis',
                        rotateLabels: -45,
                    //    thickValues: $scope.tickValues,
                        tickFormat: function(d){
                            //return d3.time.format('%Y/%W')(new Date(d)) //(NOT WORKING - try for Y/W format)
                            var label = new Date(d * 1000);
                            //return d3.format('d')(label);
                            return label.toISOString().substring(0, 10);
                        }
                    },
                    x2Axis: {
                        tickFormat: function(d){
                            //return d3.time.format('%Y/%W')(new Date(d)) //(NOT WORKING - try for Y/W format)
                            var label = new Date(d * 1000);
                            //return d3.format('d')(label);
                            return label.toISOString().substring(0, 10);
                        }
                        //rotateLabels: -45
                        /*tickFormat: function(d){
                            //return d3.time.format('%Y/%W')(new Date(d)) //(NOT WORKING - try for Y/W format)
                            return d3.format('d')(d);
                        }*/
                    },
                    yAxis: {
                        axisLabel: 'Y Axis',
                        tickFormat: function(d){
                            return d3.format('d')(d);
                        },
                        rotateYLabel: false
                    },
                    y2Axis: {
                        tickFormat: function(d){
                            return d3.format('d')(d);
                        }
                    }
                }
            };
        $scope.name = "";
        $scope.load_data = function(region_id, name) {
            $scope.name = name;
			$http.get("/api/get_age_by_time/" + region_id + "/WORKDAY").then(function(response) {
                $scope.ccData = response.data.data;
            });
            $http.get("/api/get_age_by_time/" + region_id + "/WEEKEND").then(function(response) {
                $scope.rcData = response.data.data;
            });
        };
        console.log($stateParams.automaton_id);
        if (!isNaN($stateParams.automaton_id)) {
            
            $scope.load_data($stateParams.automaton_id, $stateParams.name);
        }
		})
